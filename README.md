# Bitbucket Pipelines Pipe:  Pagely Deploy to VPS

Deploy code from your build in BitBucket Pipelines to a Pagely app.

## Variables

| Name                        | Requirement | Description                                                                                                                                                      |
|-----------------------------|-------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `PAGELY_INTEGRATION_SECRET` | _required_  | Authentication token. Create a CI Integration at [Atomic](https://atomic.pagely.com/account/integrations) to get this.                                           |
| `PAGELY_INTEGRATION_ID`     | _required_  | Unique ID for the integration found in Atomic.                                                                                                                   |
| `PAGELY_APP_ID`             | _required_  | Numeric ID of the app you want to deploy to, available in Atomic.                                                                                                |
| `PAGELY_DEPLOY_DEST`        | _required_  | Set the subdirectory to deploy to within the app. `/httpdocs` would be the web root of your app. Examples: `/httpdocs`, `/httpdocs/wp-content/plugins/my-plugin` |
| `PAGELY_WORKING_DIR`        | _optional_  | The directory that you want deployed. Defaults to `$PWD`.                                                                                                        |

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
  - pipe: pagely/pagely-vps-deploy:1
    variables:
      PAGELY_DEPLOY_DEST: $PAGELY_DEPLOY_DEST
      PAGELY_INTEGRATION_SECRET: $PAGELY_INTEGRATION_SECRET
      PAGELY_INTEGRATION_ID: $PAGELY_INTEGRATION_ID
      PAGELY_APP_ID: $PAGELY_APP_ID
      PAGELY_WORKING_DIR: $BITBUCKET_CLONE_DIR
```

## Examples

```yaml
  - pipe: pagely/pagely-vps-deploy:1
    variables:
      PAGELY_DEPLOY_DEST: "/httpdocs/wp-content/plugins/myplugin"
      PAGELY_INTEGRATION_SECRET: $PAGELY_INTEGRATION_SECRET
      PAGELY_INTEGRATION_ID: $PAGELY_INTEGRATION_ID
      PAGELY_APP_ID: $PAGELY_APP_ID
      PAGELY_WORKING_DIR: $BITBUCKET_CLONE_DIR
```

## Support

Support tickets can be created from within the [Pagely Atomic](https://atomic.pagely.com) dashboard
